# PokemonApp

This project is a Pokemon catalogue web application. The application is built with Angular Framework and we have used a pokemon API to fetch the data. 
The API: http://pokeapi.co/

The user can login to the application, read about the different Pokemon and collect them.

The application consist of 4 different pages:
- Landing page
- Trainer page
- Pokemon catalogue
- Pokemon detail

## Landing page/ Login

The user is presented with a landing page where they can input their "Trainer name". Trainers can collect Pokemon to train. The trainer names is stores in local storage. 

## Trainer page

The trainer page displays all the different Pokemon the trainer has collected.


## Pokemon catalogue

This page is only accessed after the user have entered their trainer name.  The Pokemon catalogue is a list of all the different Pokemon with a name and an image. Each pokemon is clickable and takes the user to the pokemon detail page.

## Pokemon detail

The Pokemon detail page display the image of the chosen pokemon along with abilities, height, weight and types. It also has a collect button for the logged in trainer to collect the Pokemon. The collected Pokemon is stores locally. After it has been collected it is displayed in the collected Pokemon trainer page. 

Information in the Pokemon detail page:

Base stats: 
- image
- type
- base stats
- name

Profile:
- height
- weight
- abilities
- base experience

Moves:
- a list of Moves

created by:

Sarah Fagerhol Thorstensen
Maren Ytterdal Krogsrud

