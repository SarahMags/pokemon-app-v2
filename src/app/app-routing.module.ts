import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PokemonContainer } from './features/pokemon/containers/pokemon/pokemon.container';
import { PokemonDetailContainer } from './features/pokemon-detail/containers/pokemon-detail/pokemon-detail.container';
import { LoginContainer } from './features/login/containers/login/login.container';
import { NotFoundContainer } from './features/not-found/containers/not-found/not-found.container';
import { TrainerContainer } from './features/trainer/containers/trainer/trainer.container';
import { SessionGuard } from './guards/session.guard';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login'
  },
  {
    path: 'login',
    component: LoginContainer
  },
  { path: 'pokemon',
    component: PokemonContainer,
    canActivate: [SessionGuard] // Only available when logged in
  },
  {
    path: 'pokemon/:name',
    component: PokemonDetailContainer,
    canActivate: [SessionGuard] // Only available when logged in
  },
  {
    path: 'trainer',
    component: TrainerContainer,
    canActivate: [SessionGuard] // Only available when logged in
  },
  {
    path: '**',
    component: NotFoundContainer
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
