import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NavbarComponent } from './shared/components/navbar/navbar.component';
import { AppContainerComponent } from './shared/components/container/container.component';
import { LoginContainer } from './features/login/containers/login/login.container';
import { PokemonContainer } from './features/pokemon/containers/pokemon/pokemon.container';
import { PokemonDetailContainer } from './features/pokemon-detail/containers/pokemon-detail/pokemon-detail.container';
import { TrainerContainer } from './features/trainer/containers/trainer/trainer.container';
import { NotFoundContainer } from './features/not-found/containers/not-found/not-found.container';
import { LoginFormComponent } from './features/login/login-form/login-form.component';
import { HttpClientModule } from '@angular/common/http';
import { PokemonGridComponent } from './features/pokemon/components/pokemon-grid/pokemon-grid.component';
import { PokemonProfileHeaderComponent } from './features/pokemon-detail/components/pokemon-profile-header/pokemon-profile-header.component';

@NgModule({
  declarations: [
    AppComponent,
    // Containers
    LoginContainer,
    PokemonContainer,
    PokemonDetailContainer,
    TrainerContainer,
    NotFoundContainer,
    // Components
    NavbarComponent,
    AppContainerComponent,
    LoginFormComponent,
    PokemonGridComponent,
    PokemonProfileHeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
