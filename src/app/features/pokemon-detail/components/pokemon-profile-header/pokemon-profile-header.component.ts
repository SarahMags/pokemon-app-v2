import { Component, Input } from '@angular/core';
import { Pokemon } from '../../../../models/pokemon.model';
import { setStorage } from '../../../../utils/storage.utils';

@Component({
  selector: 'app-pokemon-profile-header',
  templateUrl: './pokemon-profile-header.component.html'
})
export class PokemonProfileHeaderComponent {
  @Input() pokemon: Pokemon;

  onCollectClick() { // Collecting pokemon and storing in localstorage
    if (confirm('Would you like to collect this pokemon?')){
      setStorage(String(this.pokemon.id), this.pokemon.name);
    }
  }
}
