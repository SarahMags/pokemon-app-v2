import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PokemonDetailService } from '../../services/pokemon-detail.service';
import { Pokemon } from '../../../../models/pokemon.model';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.container.html'
})
export class PokemonDetailContainer implements OnInit{

  private readonly pokemonName: string = '';

  constructor(private readonly route: ActivatedRoute, private readonly pokemonDetailService: PokemonDetailService) {
    this.pokemonName = this.route.snapshot.paramMap.get('name'); // Sets the name from the cache
  }

  ngOnInit(): void {
    this.pokemonDetailService.fetchPokemonByName(this.pokemonName); // Gets the pokemon with details via pokemon name
  }

  get pokemon(): Pokemon {
    return this.pokemonDetailService.pokemon;
  }
}
