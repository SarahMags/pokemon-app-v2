import { Injectable } from '@angular/core';
import { Pokemon } from '../../../models/pokemon.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { map } from 'rxjs/operators';

const { pokeAPI } = environment;

@Injectable({
  providedIn: 'root'
})

export class PokemonDetailService {

  public pokemon: Pokemon;

  constructor(private readonly http: HttpClient) {
  }

  // Uses name in the api route to get a specific pokemon
  public fetchPokemonByName(name: string): void{
    this.http.get<Pokemon>(`${pokeAPI}/pokemon/${name}`)
      .pipe(
        map((pokemon: Pokemon) => ({
      ...pokemon,
      image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${pokemon.id}.png`
        }))
      )
      .subscribe((pokemon: Pokemon) => {
        this.pokemon = pokemon;
      });
  }
}
