import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { map, shareReplay } from 'rxjs/operators';
import { Pokemon } from '../../../models/pokemon.model';
import { PokemonResponse } from '../../../models/pokemon-response.model';

const { pokeAPI } = environment; // api base url

@Injectable({
  providedIn: 'root'
})

export class PokemonService{ // Handles fetching of pokemon from API, cashes to avoid many api calls

  private readonly pokemonCache$;
  public pokemon: Pokemon[] = []; // Empty array of pokemon objects
  public error: string = '';

  constructor(private readonly http: HttpClient) {
    this.pokemonCache$ =
      this.http.get<PokemonResponse>(`${pokeAPI}/pokemon`)
        .pipe(shareReplay(1)); // storing as cache
  }

  fetchPokemon(): void{  // Fetching pokemon
    this.pokemonCache$
      .pipe(
          map((response: PokemonResponse) => {
            return response.results.map((pokemon: Pokemon) => ({
              ...pokemon,
              ...this.getIdAndImage(pokemon.url)
            }));
          })
      )
      .subscribe(
        (pokemon: Pokemon[]) => {
          this.pokemon = pokemon;
        },
        (errorResponse: HttpErrorResponse) => {
          this.error = errorResponse.message;
        }
      );
  }

  private getIdAndImage(url: string): any { // Getting image from id
    const id = url.split('/').filter( Boolean ).pop();
    return {
      id: Number(id),
      image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${ id }.png`};
  }
}
