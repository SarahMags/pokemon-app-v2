import { Component } from '@angular/core';
import { PokemonService } from '../../services/pokemon.service';
import { Pokemon } from '../../../../models/pokemon.model';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.container.html'
})

export class PokemonContainer{

  constructor(private readonly pokemonService: PokemonService) {
  }

  get pokemon(): Pokemon[] {
    return this.pokemonService.pokemon;
  }

  ngOnInit(): void{
    this.pokemonService.fetchPokemon(); // Fetching pokemon

  }
}
