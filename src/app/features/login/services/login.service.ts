import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginState } from '../state/login.state';
import { Observable } from 'rxjs';
import { User } from '../../../shared/models/user.model';
import { getStorage, setStorage } from '../../../utils/storage.utils';
import { StorageKeys } from '../../../enums/storage-keys.enum';
import { Trainer } from '../../../models/trainer.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private readonly http: HttpClient, private readonly loginState: LoginState) {
  }


  private setTrainer(trainer: any): void{
    setStorage(StorageKeys.TRAINER, trainer);
  }

  public user$(): Observable<User> {
    return this.loginState.getUser$();
  }

  public login(username: string): void {
    if (username !== '') {
      this.loginState.setUser({username, pokemon: []});
      setStorage(StorageKeys.TRAINER, username);
    }
  }
}
