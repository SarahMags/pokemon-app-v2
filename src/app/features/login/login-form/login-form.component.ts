import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../../../shared/models/user.model';
import {Subscription} from 'rxjs';
import {LoginService} from '../services/login.service';
import {getStorage} from '../../../utils/storage.utils';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styles: [
    `label, input {
      display: block;
      width: 100%;
      margin-bottom: 1em;
    }
    `
  ]
})
export class LoginFormComponent implements OnInit, OnDestroy {

  @Output() successful: EventEmitter<User> = new EventEmitter<User>();

  public username: string = '';

  private user$: Subscription;
  loading: boolean = false;

  loginForm: FormGroup = new FormGroup({
    trainerName: new FormControl('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(20),
      Validators.pattern(/[a-zA-Z]/)
    ])
  });

  constructor(private readonly loginService: LoginService) {
    this.user$ = this.loginService.user$().subscribe((user: User) => {
      if (user === null) {
        return;
      }
      this.successful.emit(user);
    });
  }

  ngOnInit(): void {
    const existingTrainer = getStorage('pk-tr');
    if (existingTrainer){
      this.successful.emit();
    }

  }

  get trainerName(): AbstractControl {
    return this.loginForm.get('trainerName');
  }

  onStartClick() {
    this.loginService.login(this.username);
    // this.loading = true;
  }
  ngOnDestroy(): void {
    this.user$.unsubscribe();
  }
}
