import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../../../shared/models/user.model';


@Injectable({
  providedIn: 'root'
})

export class LoginState {
  private readonly user$: BehaviorSubject<User> = new BehaviorSubject<User>(null);

  public getUser$(): Observable<User> {
    return this.user$.asObservable();
  }

  public setUser(user: User): void {
    this.user$.next(user);
  }

}

