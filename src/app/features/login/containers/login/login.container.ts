import { Component } from '@angular/core';
import { AppRoutes } from '../../../../enums/app-routes.enum';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.container.html'
})
export class LoginContainer{
  constructor(private readonly router: Router) {
  }
  handleLoginSuccess(): void {
    this.router.navigateByUrl( AppRoutes.POKEMON ); // Redirects to pokemon page if logged in
  }
}
