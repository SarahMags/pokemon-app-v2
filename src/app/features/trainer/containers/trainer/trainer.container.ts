import { Component, Input, OnInit } from '@angular/core';
import { getStorage } from '../../../../utils/storage.utils';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.container.html'
})
export class TrainerContainer implements OnInit{
  @Input() returnNames = '';
  private pokemonNames: [] = [];

  ngOnInit(): void {
    // Gets the values from localstorage without knowing their keys
    for (const key in localStorage){
      if (key !== 'pk-tr') { // We just want the pokemon, so ignore pokemon trainer
        this.pokemonNames.push(getStorage(key)); // Push values into array
      }
    }
    // The array turned out weird, so make it into a string and remove the commas
    this.returnNames = this.pokemonNames.toString().replace(/,/g, ' ');
  }
}
