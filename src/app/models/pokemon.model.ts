export interface Pokemon {
  id?: number;
  name: string;
  url: string;
  image?: string;
  weight?: number;
  height?: number;
  types?: PokemonType[];
  stats?: PokemonStat[];
  sprites?: any[];
  abilities?: Ability[];
  base_experience?: number;
  moves?: PokemonMoves[];
}

export interface PokemonMoves{
  move: {name: string; url: string};
  version_group_details: [];
}

export interface Ability {
  ability: {name: string; url: string};
  is_hidden: boolean;
  slot: number;
}

export interface PokemonSprite{
  back_shine: string;
  front_shine: string;
  other: PokemonSpriteOther;
}

export interface PokemonSpriteOther {
  dream_world: any;
  'official-artwork': PokemonSpriteOfficial;
}

export interface PokemonSpriteOfficial{
  front_default: string;
}

export interface PokemonType {

  slot: number;
  type: PokemonTypeType;
}

export interface PokemonTypeType{
  name: string;
  url: string;
}

export interface PokemonStat {
  base_stat: number;
  effort: number;
  stat: PokemonStatStat;
}

export interface  PokemonStatStat{
  name: string;
  url: string;
}


