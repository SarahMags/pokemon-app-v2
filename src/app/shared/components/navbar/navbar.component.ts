import { Component } from '@angular/core';
import { AppRoutes } from '../../../enums/app-routes.enum';
import { SessionService } from '../../services/session.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent{
  public appRoutes = AppRoutes;

  constructor(private readonly sessionService: SessionService){
    }

    get hasActiveSession(): boolean { // Used to check if logged in
      return this.sessionService.active();
    }

}
