import { Pokemon } from '../../models/pokemon.model';
// First version of Trainer, used a bit with login logic
export interface User {
  username: string;
  pokemon: Pokemon[];
}
