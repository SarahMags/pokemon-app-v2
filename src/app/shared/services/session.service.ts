import { Injectable } from '@angular/core';
import { getStorage } from '../../utils/storage.utils';
import { StorageKeys } from '../../enums/storage-keys.enum';

@Injectable({
  providedIn: 'root'
})

export class SessionService{
// Used to check if logged in
  active(): boolean{

    const trainer = getStorage(StorageKeys.TRAINER);
    return Boolean (trainer); // Returns true or false depending on if trainer is stored in localstorage
  }
}

